package DemoqaTests;

import Base.TestBase;
import Pages.MenuPage;
import Pages.SliderPage;
import Utilities.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/* Test 2
 * 1. Przejsć do menu Slider
 * 2. Przesunąć slider do wartości określonej w teście
 * 3. Sprawdzić czy wartość Minimum number of bedrooms jest równa oczekiwanej
 *
 * Dla chętnych:
 * Rozwiązać zadanie na 2 rózne sposoby (dopytać trenera)
 *
 */
public class Slider extends TestBase {
    SliderPage sliderPage;
    DataGenerator generator;
    MenuPage menu;

    @BeforeMethod
    public void sliderBase() {
        menu = new MenuPage(driver);
        sliderPage = new SliderPage(driver);
    }

    @Test
    public void sliderTest() {
        menu.openSlider();
        sliderPage.moveSliderTo(5)
                .moveSliderTo(7)
                .moveSliderTo(7)
                .moveSliderTo(3);
        Assert.assertEquals(sliderPage.getCurrentLocation(), 3);

    }
}
