package DemoqaTests;

import Base.TestBase;
import Pages.DroppablePage;
import Pages.MenuPage;
import Utilities.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/* Test 3
 * 1. Przejsć do menu Droppable (pozostajemy w default functionality)
 * 2. Przesunąć obiekt 'drag me' i przesunąc go na 'drop here'
 * 3. Sprawdzić czy zmienił sie napis z 'drop here' na 'dropped!'
 *
 * Dla chętnych:
 * Shopping cart demo (przenieść po 1 rodzaju produktu do koszyka)
 *
 * Tips:
 * Poszukać jak symulować akcje (np. nacisniecie przycisku myszy , przytrzymanie przycisku, przesunięcie kursora)
 *
 */
public class Droppable extends TestBase {
    DroppablePage droppablePage;
    DataGenerator generator;
    MenuPage menu;

    @BeforeMethod
    public void droppableBase() {
        menu = new MenuPage(driver);
        droppablePage = new DroppablePage(driver);
    }

    @Test
    public void droppableTest() {
        menu.openDroppable();
        droppablePage.dragAndDropElement();
        Assert.assertEquals(droppablePage.getDropText(), "Dropped!");
    }

}