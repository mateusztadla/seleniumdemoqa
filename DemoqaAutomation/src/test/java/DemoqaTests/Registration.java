package DemoqaTests;

import Base.TestBase;
import Pages.MenuPage;
import Pages.RegistrationPage;
import Utilities.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/* Test 1
 * 1. Przejsć do menu rejestracji
 * 2. Wypełnić wszystkie pola formularza
 * 3. Kliknąć przycisk 'Submit'
 * 4. Weryfikacja czy rejestracja się powiodła
 * 5. Ponownie kliknąć przycisk 'Submit'
 * 6. Weryfikacja błedu 'Username already exists'
 *
 * Uwagi: całość opieryamy na POP, tworzymy odpowiednie PO, matody wstawiające wartości muszą być
 * sparametryzowane i odpowiednie dane przykazujem z poziomu testu. Username oraz mail powinien być losowy
 */
public class Registration extends TestBase {
    RegistrationPage registrationPage;
    DataGenerator generator;
    MenuPage menu;

    @BeforeMethod
    public void registrationBase() {
        generator = new DataGenerator();
        menu = new MenuPage(driver);
        registrationPage = new RegistrationPage(driver);

    }

    @Test
    public void registrationTest() {
        menu.openRegistration();

        registrationPage.setFirstName("Jan")
                .setLastName("Kowalski")
                .selectHobby()
                .setNumber("1234567890")
                .setUserName(generator.getRandomUserName())
                .setEmail(generator.getRandomEmail())
                .setPassword("qweasdzxc123")
                .setConfirmPassword("qweasdzxc123")
                .submit();
        Assert.assertEquals(registrationPage.getResultMessage(), "Thank you for your registration");

        registrationPage.setPassword("qweasdzxc123")
                .setConfirmPassword("qweasdzxc123")
                .submit();
        Assert.assertEquals(registrationPage.getResultMessage(), "Error: Username already exists");

    }

}
