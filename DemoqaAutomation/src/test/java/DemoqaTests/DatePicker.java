package DemoqaTests;

import Base.TestBase;
import Pages.DatapickerPage;
import Pages.MenuPage;
import Utilities.DataGenerator;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/* Test 4
 * 1. Przejsć do menu datepicker
 * 2. Kliknąć na pole wyboru daty (z menu 'default functionality')
 * 3. Wybrać datę (date przekazujemy jako parametr z poziomu testu)
 * 4. Sprawdzić czy wybrana data została wyświetlona w polu daty
 *
 * Uwagi:
 * aby nie komplikować dodatkowo logiki testu wystarczy aby przekazywać do niego
 * datę w postaci 3 wartości tekstowych: rok, miesiąc i dzień
 *
 * Dla chętnych:
 * Automatyzacja opcji  "Display month & year"
 *
 * Tips:
 * Usuwanie liczb z tekstu: "123asd345qwe".replaceAll("\\D",""); => 123345
 * Usuwanie liter z tekstu: "123asd345qwe".replaceAll("\\d",""); => asdqwe
 *
 */
public class DatePicker extends TestBase {
    DatapickerPage datapickerPage;
    DataGenerator generator;
    MenuPage menu;

    @BeforeMethod
    public void datePickerBase() {
        generator = new DataGenerator();
        menu = new MenuPage(driver);
        datapickerPage = new DatapickerPage(driver);
    }

    @Test
    public void nextYear() throws InterruptedException {
        menu.openDatePicker();
        datapickerPage.clickData();
        datapickerPage.setDate(5, "January", 2019);
    }

    @Test
    public void previousYear() throws InterruptedException {
        menu.openDatePicker();
        datapickerPage.clickData();
        datapickerPage.setDate(5, "December", 2017);
    }

    @Test
    public void thisYear() throws InterruptedException {
        menu.openDatePicker();
        datapickerPage.clickData();
        datapickerPage.setDate(5, "June", 2018);
    }

    @Test
    public void thisMonth() throws InterruptedException {
        menu.openDatePicker();
        datapickerPage.clickData();
        datapickerPage.setDate(5, "April", 2018);
    }

    @Test
    public void thisDay() throws InterruptedException {
        menu.openDatePicker();
        datapickerPage.clickData()
                .setDate(29, "April", 2018);
    }
}