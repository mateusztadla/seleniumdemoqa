package Pages;

import Utilities.WebElementManipulator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DatapickerPage extends WebElementManipulator {

    List<String> months = new ArrayList<>(
            Arrays.asList("January", "February", "March", "April",
                    "May", "June", "July", "August", "September",
                    "October   ", "November", "December"));

    @FindBy(css = "a[data-handler='prev']")
    private WebElement leftArrow;

    @FindBy(css = "a[data-handler='next']")
    private WebElement rightArrow;

    @FindBy(className = "ui-datepicker-title")
    private WebElement currentDate;

    @FindBy(id = "datepicker1")
    private WebElement dataInput;

    @FindBys(@FindBy(css = "td[data-handler='selectDay']"))
    private List<WebElement> days;

    public DatapickerPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void setDate(int day, String month, int year) throws InterruptedException {
        if (compareYears(year)) {
            if (compareMonths(month)) {
                selectDay(day);
                return;
            }
            if (getIndexOfMonth(getCurrnetMonth()) > getIndexOfMonth(month)) {
                moveLeft(day, month, year);
                return;
            }
            moveRight(day, month, year);
            return;
        }
        if (year > getCurrnetYear()) {
            moveRight(day, month, year);
            return;
        }
        moveLeft(day, month, year);
        return;
    }

    public int getIndexOfMonth(String month) {
        return months.indexOf(month);
    }

    public String getData() {
        return dataInput.getText();
    }

    public DatapickerPage clickData() {
        dataInput.click();
        elementIsVisible(currentDate);
        return this;
    }

    public void selectDay(int day) {
        days.get(day - 1).click();
        elementDisapeard(currentDate);
    }

    public void moveLeft(int day, String month, int year) throws InterruptedException {
        while (!compareYears(year) && !compareMonths(month)) {
            leftArrow.click();
            Thread.sleep(500);
        }
        selectDay(day);
    }

    public void moveRight(int day, String month, int year) throws InterruptedException {
        while (!compareYears(year) && !compareMonths(month)) {
            rightArrow.click();
            Thread.sleep(500);
        }
        selectDay(day);
    }

    public String getCurrnetMonth() {
        return currentDate.getText()
                .replaceAll("\\d", "").trim();
    }

    public int getCurrnetYear() {
        return Integer.parseInt(currentDate.getText()
                .replaceAll("\\D", ""));
    }

    public boolean compareMonths(String month) {
        return month.equals(getCurrnetMonth());
    }

    public boolean compareYears(int year) {
        return year == getCurrnetYear();
    }
}
