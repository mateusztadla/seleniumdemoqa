package Pages;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenuPage extends WebElementManipulator {

    @FindBy(id = "menu-registration")
    private WebElement registration;
    @FindBy(linkText = "Droppable")
    private WebElement droppable;
    @FindBy(linkText = "Datepicker")
    private WebElement datePicker;
    @FindBy(linkText = "Slider")
    private WebElement slider;

    public MenuPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void openRegistration() {
        registration.click();
    }

    public void openDroppable() {
        droppable.click();
    }

    public void openSlider() {
        slider.click();
    }

    public void openDatePicker() {
        datePicker.click();
    }
}
