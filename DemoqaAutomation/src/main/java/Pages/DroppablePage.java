package Pages;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DroppablePage extends WebElementManipulator {
    @FindBy(id = "droppableview")
    private WebElement droppableArea;

    @FindBy(id = "draggableview")
    WebElement dragableElement;

    public DroppablePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    public void dragAndDropElement(){
        Actions actionDriver = new Actions(driver);
        Action dragAndDrop = actionDriver.clickAndHold(dragableElement)
                .moveToElement(droppableArea)
                .release(dragableElement)
                .build();
        dragAndDrop.perform();
    }
    public String getDropText(){
        return droppableArea.getText();
    }
}
