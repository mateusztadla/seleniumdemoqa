package Pages;

import Utilities.WebElementManipulator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage extends WebElementManipulator {
    @FindBy(name = "first_name")
    private WebElement firstNameInput;

    @FindBy(name = "last_name")
    private WebElement lastNameInput;

    @FindBy(css = "input[value='single']")
    private WebElement maritalStatusSingle;

    @FindBy(css = "input[value='married']")
    private WebElement maritalStatusMarried;

    @FindBy(css = "input[value='divorced']")
    private WebElement maritalStatusDivorced;

    @FindBy(css = "input[value='dance']")
    private WebElement hobbyDance;

    @FindBy(css = "input[value='reading']")
    private WebElement hobbyReading;

    @FindBy(css = "input[value='cricket']")
    private WebElement hobbyCricket;

    @FindBy(css = "select[id='dropdown_7']")
    private WebElement countrySelect;

    @FindBy(css = "select[id='mm_date_8']")
    private WebElement monthSelect;

    @FindBy(css = "select[id='dd_date_8']")
    private WebElement daySelect;

    @FindBy(css = "select[id='yy_date_8']")
    private WebElement yeatSelect;

    @FindBy(css = "input[id='phone_9']")
    private WebElement numberInput;

    @FindBy(css = "input[id='username']")
    private WebElement userNameInput;

    @FindBy(name = "e_mail")
    private WebElement emailInput;

    @FindBy(id = "description")
    private WebElement descriptionInput;

    @FindBy(css = "input[id='password_2']")
    private WebElement passwordInput;

    @FindBy(css = "input[id='confirm_password_password_2']")
    private WebElement confirmPasswordInput;

    @FindBy(name = "pie_submit")
    private WebElement submitButton;

    @FindBy(css = "p[class^='piereg']")
    private WebElement message;

    public RegistrationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public RegistrationPage submit() {
        submitButton.click();
        return this;
    }

    public String getResultMessage() {
        return message.getText();
    }

    public RegistrationPage selectHobby() {
        hobbyDance.click();
        return this;
    }

    public RegistrationPage setFirstName(String firstName) {
        firstNameInput.sendKeys(firstName);
        return this;

    }

    public RegistrationPage setNumber(String number) {
        numberInput.sendKeys(number);
        return this;

    }

    public RegistrationPage setLastName(String lastName) {
        lastNameInput.sendKeys(lastName);
        return this;

    }

    public RegistrationPage setUserName(String userName) {
        userNameInput.sendKeys(userName);
        return this;

    }

    public RegistrationPage setEmail(String email) {
        emailInput.sendKeys(email);
        return this;

    }

    public RegistrationPage setPassword(String password) {
        passwordInput.sendKeys(password);
        return this;

    }

    public RegistrationPage setConfirmPassword(String password) {
        confirmPasswordInput.sendKeys(password);
        return this;

    }
}
