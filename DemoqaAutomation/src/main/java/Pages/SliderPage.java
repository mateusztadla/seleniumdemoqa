package Pages;

import Utilities.WebElementManipulator;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SliderPage extends WebElementManipulator {

    @FindBy(id = "amount1")
    private WebElement sliderText;

    @FindBy(css = ".ui-slider-handle")
    private WebElement slider;

    public SliderPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public int getCurrentLocation() {
        return Integer.parseInt(sliderText.getAttribute("value"));
    }

    public SliderPage moveSliderTo(int position) {
        if (position == getCurrentLocation()) {
            return this;
        }

        if (position > getCurrentLocation()) {
            move(position - getCurrentLocation(),
                    Keys.ARROW_RIGHT);
            return this;
        }
        move(getCurrentLocation() - position,
                Keys.ARROW_LEFT);
        return this;
    }

    public void move(int count, Keys key) {
        for (int i = 0; i < count; i++) {
            slider.sendKeys(key);
        }
    }
}
