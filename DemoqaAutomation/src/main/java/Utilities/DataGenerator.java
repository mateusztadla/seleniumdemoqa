package Utilities;

import java.util.concurrent.ThreadLocalRandom;

public class DataGenerator {
    public int getRandomNumber(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public String getRandomUserName() {
        return "user" + getRandomNumber(1000, 9999);
    }

    public String getRandomEmail() {
        return "mail" + getRandomNumber(1000, 9999) + "@wp.pl";
    }
}
